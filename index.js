"use strict";

/*    1.   Опишіть, як можна створити новий HTML тег на сторінці.
        
            const newTag = document.createElement('tagName');
            document.body.appendChild(newTag);

      2.    Опишіть, що означає перший параметр функції insertAdjacentHTML 
            і опишіть можливі варіанти цього параметра.

            Перший параметр функції insertAdjacentHTML вказує на місце, 
            куди буде вставлятися вміст, який визначений другим параметром. 
            Можливі варіанти першого параметра:

            beforebegin: вставляє вміст перед початком цільового елемента.
            afterbegin: вставляє вміст в середину цільового елемента, перед першим дочірнім елементом.
            beforeend: вставляє вміст в середину цільового елемента, після останнього дочірнього елемента.
            afterend: вставляє вміст після цільового елемента.
            


      3.    Як можна видалити елемент зі сторінки?
            
            remove()
            parentNode.removeChild()

            заміна всього HTML-коду елемента, який потрібно видалити, на порожній рядок:

            const element = document.getElementById("myElement");
            element.outerHTML = "";
                 */

function createUl(arr, parent = document.body) {
  const ul = document.createElement("ul");

  parent.appendChild(ul);

  const listItems = arr.map((el) => {
    const li = document.createElement("li");

    if (Array.isArray(el)) {
      li.appendChild(createUl(el, ul));
    } else {
      li.textContent = el;
    }
    return li;
  });

  listItems.forEach((li) => {
    ul.appendChild(li);
  });

  return ul;
}

const timer = document.createElement("div");
document.body.appendChild(timer);

// Функція, яка відображає таймер зворотнього відліку
function countdown() {
  let count = 3;
  timer.innerText = count;

  const interval = setInterval(() => {
    count--;
    timer.innerText = count;
    if (count === 0) {
      clearInterval(interval);
      document.body.innerHTML = "";
    }
  }, 1000);
}

setTimeout(countdown);

createUl([
  "Kharkiv",
  "Kiev",
  ["Borispol", "Irpin"],
  "Odessa",
  "Lviv",
  "Dnieper",
]);
